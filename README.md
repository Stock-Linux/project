# project

This is a simple repo to store our vision of Stock Linux.

# A global vision of the project

Let's start with a great question: Why do I want to create a new distro ?

I want to create Stock Linux to provide a better experience of Linux to the users, with a relatively "easy-to-use" but **powerful** package manager which can satisfy little hackers with some experience who don't really like compilation etc but like a lightweight system and which can satisfy power-users like me, and many others who love compiling packages or customizing the existing ones. Our package manager, **squirrel**, handles that and this is amazing. Now let's talk about the release cycle of Stock Linux which is a little bit original.
I think Stock Linux should be a sort of rolling release but at the same time it can probably be a stable release distro. Indeed, Stock Linux will have multiple package updates in the same week and this cannot satisfy everyone and I agree. BUT, we will create 2 branches of the distro, the main branch will be a rolling release and the other branch will be a "stable" release which will have a delay of 1 month between updates. WE CANNOT SAY THAT THE STABLE BRANCH WILL BE STABLE.

So, to conclude, the main points of Stock Linux are:

- The distro itself is a rolling release, but with a "stable" branch which will have a 1 month delay between updates.
- The distro mainly aims at power-users who like customizing their system and their packages but the distro can satisfy users with a little bit of experience who like rolling releases and lightweight systems but this is at their own risk.